<?php
namespace Doc2Pdf\Tools;

/**
* Общий класс для утилит вызываемых из консоли
* @author E.Prokhorov
* @date 26.06.2017
*/
class Package
{
	/**
	* @var string
	*/
	protected $_packageName;
	
	/** 
	* Конструктор объекта, сразу проверяет наличие нужных функций и пакетов
	*/
	public function __construct()
	{
		$this->_checkPackage();
	}

	/**
	* Проверяет установлен ли пакет в системе
	* @return boolean
	*/
	private function _checkPackage()
	{
		if ($this->_isShellAvalible()) {
			$response = shell_exec('rpm -q '.$this->_packageName);
			if (trim($response) == 'package '.$this->_packageName.' is not installed') {
				throw new \Exception('Package "'.$this->_packageName.'" is not installed on server', 2);
			} else {
				return true;
			} 
		}
	}

	/**
	* Проверяет возможность вызова функции shell_exec
	* @return boolean
	*/
	private function _isShellAvalible()
	{
		$disabled = explode(',', ini_get('disable_functions'));
		$needFunc = 'shell_exec';

		if (in_array($needFunc, $disabled) || !function_exists($needFunc)) {
			throw new \Exception('Function "'.$needFunc.'" is not avalible', 1);
		} else {
			return true;
		}		
	}

	/**
	* Проверяет существование и возможности записи в директорию
	* @param $path
	* @return boolean
	*/
	protected function _checkDir($path)
	{
		if (!is_writable($path)) {
			throw new \Exception('Directory "'.$path.'" not exisits or not writeable', 3);
		} else {
			return true;
		}
	}
}
?>