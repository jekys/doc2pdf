<?php
namespace Doc2Pdf\Tools;

/**
* Класс для работы с консольной утилитой pdftk (PDFtoolkit)
* @author E.Prokhorov
* @date 26.06.2017
*/
class PDFtk extends Package
{
	/**
	* @var string
	*/
	protected $_packageName = 'pdftk';

	/**
	* @var array
	*/
	private $_arDirections = [
		'north', // 0 degree
		'south', // 180 degree
		'east',  // 90 degree
		'west',  // 270 degree
		'left',  // -90 degree
		'right', // +90 degree
		'down',  // +180 degree
	];

	/**
	* @var array
	*/
	private $_arOperations = [
		'cat',
		'shuffle',
		'burst',
		'rotate',
		'generate_fdf',
		'fill_form',
		'background',
		'multibackground',
		'stamp',
		'multistamp',
		'dump_data',
		'dump_data_utf8',
		'dump_data_fields',
		'dump_data_fields_utf8',
		'dump_data_annots',
		'update_info',
		'update_info_utf8',
		'attach_files',
		'unpack_files'
	];

	/**
	* Проверяет правильность указания направления поворота
	* @param string
	* @return boolean
	*/
	private function _checkDirection($direction)
	{
		if (!in_array($direction,$this->_arDirections)) {
			throw new \Exception('Wrong PDFtk rotate direction', 7);
		} else {
			return true;
		}
	}	

	/**
	* Проверяет является ли операция допустимой
	* @param string $operation
	* @return boolean
	*/
	private function _checkOperation($operation)
	{
		if (!in_array($operation,$this->_arOperations)) {
			throw new \Exception('Wrong PDFtk operation', 8);
		} else {
			return true;
		}
	}

	/**
	* Поворачивает страницы в PDF-документе указанном в $input и сохрянетя в $output
	* @param string $input
	* @param string $output
	* @param string $direction
	* @param int $beginNum
	* @param int $endNum
	* @return boolean
	*/
	public function rotate($input,$output,$direction,$beginNum,$endNum = '')
	{
		if ($this->_checkDirection($direction)) {
			$arguments = $beginNum.$direction;
			
			if (!empty($endNum)) {
				$arguments .= ' '.$endNum.'-end';
			}

			return $this->exec($input,'cat',$arguments,$output);
		}
		
	}

	/**
	* Добавляет PDF файл указанный в $stamp как фон в файла в $input и сохрянет в $output
	* @param string $input
	* @param string $stamp
	* @param string $output
	* @return boolean
	*/
	public function multibackground($input,$stamp,$output)
	{
		if (file_exists($stamp)) {
			return $this->exec($input,'multibackground',$stamp,$output);
		} else {
			throw new \Exception('File "'.$stamp.'"not exists', 9);
		}
	}

	/**
	* Вызывает в консоли PDFtk и выполняет действие указанное в $operation с аргументами в $operationArguments
	* @param string $input
	* @param string $operation
	* @param string $operationArguments
	* @param string $output
	* @return boolean
	*/
	private function exec($input,$operation,$operationArguments,$output)
	{
		if (file_exists($input)) {
			if ($this->_checkOperation($operation)) {
				$response = shell_exec('pdftk '.$input.' '.$operation.' '.$operationArguments.' output '.$output);
				if (!file_exists($output)) {
					throw new \Exception('Output file "'.$output.'"not created', 11);
				} else {
					return true;
				}
			}
		} else {
			throw new \Exception('Input file "'.$input.'"not exists', 10);
		}
	}
}
?>