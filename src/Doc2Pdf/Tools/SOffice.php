<?php
namespace Doc2Pdf\Tools;

/**
* Класс для работы с консольной утилитой soffice (LibreOffice)
* @author E.Prokhorov
* @date 26.06.2017
*/
class SOffice extends Package
{
	/**
	* @var string
	*/
	protected $_packageName = 'libreoffice';

	/**
	* @var string
	*/
	private $_outputDir = './';
	
	/**
	* @var string
	*/
	private $_outputExtention;
	
	/**
	* @var string
	*/
	private $_outputFilter;

	public function __construct()
	{
		parent::__construct();
		$this->_checkDir($this->_outputDir);
	}

	/**
	* Устанавливает директорию для записи полученных файлов
	* @param string $outputDir
	* @return object 
	*/
	public function setOutputDir($outputDir)
	{
		if (empty($outputDir)) {
			$outputDir = './';
		}

		if (substr($outputDir, -1) != '/') {
			$outputDir .= '/';
		}

		$this->_outputDir = $outputDir;

		if ($this->_checkDir($this->_outputDir)) {
			return $this;
		}
	}

	/**
	* Устанавливает расширение выходного файла
	* @var string $extention
	* @return object
	*/
	public function setExtention($extention)
	{
		//@TODO: check avalibale extentions
		$this->_outputExtention = $extention;
		return $this;
	}

	/**
	* Устанавливает фильтр рендеринга выходного файла
	* @var string $ашдеук
	* @return object
	*/
	public function setFilter($filter)
	{
		//@TODO: check avalibale filters
		$this->_outputFilter = $filter;
		return $this;
	}

	/**
	* Конвертирует файл
	* @param string $filePath
	* @return string
	*/
	public function convert($filePath)
	{
		if (empty($this->_outputExtention)) {
			throw new \Exception('Output file extention is empty', 4);
		}
		
		if (empty($this->_outputFilter)) {
			throw new \Exception('Output file filter is empty', 5);
		}

		$fileInfo = new \SplFileInfo($filePath);

		if (!$fileInfo->isFile()) {
			throw new \Exception('File "'.$filePath.'" not exists', 6);
		} else {
			$response = shell_exec('soffice --headless --norestore --writer --convert-to '.$this->_outputExtention.':'.$this->_outputFilter.' --outdir '.$this->_outputDir.' '.$filePath);

			$fileName = str_replace('.'.$fileInfo->getExtension(),'',$fileInfo->getFilename());

			if (!file_exists($this->_outputDir.$fileName.'.'.$this->_outputExtention)) {
				throw new \Exception('File "'.$filePath.'" not converted to '.strtoupper($this->_outputExtention), 7);
			} else {
				return $this->_outputDir.$fileName.'.'.$this->_outputExtention;
			}
		}
	}
}
?>

