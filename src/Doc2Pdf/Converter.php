<?php
namespace Doc2Pdf;

/**
* Класс для конвертации документов в PDF + подстановка водняного знака
* @author E.Prokhorov
* @date 26.06.2017
*/
class Converter
{
	/**
	* @var string
	*/
	private $_workDir = '/upload/converted_pdf/';
	
	/**
	* @var string
	*/
	private $_workPath;
	
	/**
	* @var int
	*/
	private $_startTime;

	/**
	* @var string
	*/
	private $_stampText;

	/**
	* @var string
	*/
	private $_extension;

	/**
	* @var boolean
	*/
	private $_cleanAfter = true;	

	public function __construct()
	{
		$this->_startTime = time();
	}

	/**
	* Устанавливает необходимость очистки директории от промежуточных файлов
	* @param bool
	* @return object
	*/
	public function cleanDirAfter($bool)
	{
		$this->_cleanAfter = $bool;
		return $this;
	}

	/**
	* Устанавливает рабочую директорию
	* @param string $dirPath
	* @return object
	*/
	public function setWorkDir($dirPath)
	{
		if (empty($dirPath)) {
			$dirPath = './';
		}

		if (substr($dirPath, -1) != '/') {
			$dirPath .= '/';
		}

		$this->_workDir = $dirPath;

		return $this;
	}

	/**
	* Устанавливает текст водного знака
	* @param string $stampText
	* @return object
	*/
	public function setStamp($stampText)
	{
		$this->_stampText = $stampText;
		return $this;
	}	

	/**
	* Проверяет существование рабочей директории и возможность записи в нее
	* Пытается создать директорию если ее не существует
	* @return boolean
	*/
	private function _checkWorkDir()
	{
		if (!is_dir($this->_workDir)) {
			var_dump(mkdir($this->_workDir,0777,true));
		}

		if (!is_writable($this->_workDir)) {
			throw new \Exception('Directory "'.$this->_workDir.'" not exisits or not writeable', 3);
		} else {
			return true;
		}
	}

	/**
	* Генерирует водный знак в виде html
	* @return boolean
	*/
	private function _makeStampHtml()
	{
		$html = '<!DOCTYPE html>
				<html>
				<head>
					<title></title>
					<style type="text/css">
						body {
							width: 100%;
							height: 100%;
							text-align: center !important;
							font-size: 60px;
							color: #ff9e9e;
						}
					</style>
				</head>
				<body>
					<br/><br/>
					<br/><br/>
					<br/><br/>
					<p>'.$this->_stampText.'</p>
				</body>
				</html>';

		return file_put_contents($this->_workPath.'_stamp.html',$html);
	}

	/**
	* Конвертирует указанный файл в PDF
	* Если вызывался метод setStamp, то текст водного знака будет проставлен на каждой странице
	* @param string $filePath
	* @return string
	*/
	public function convert($filePath, $isDiskFile = false)
	{
		$fileInfo = new \SplFileInfo($filePath);

		if (!$fileInfo->isFile()) {
			throw new \Exception('File "'.$filePath.'" not exists', 6);
		} else {
			if ($this->_checkWorkDir()) {

				$this->_workPath = $this->_workDir.md5($fileInfo->getFilename()).'_'.$this->_startTime;
				if (!$isDiskFile) {
					$this->_extension = '.'.$fileInfo->getExtension();
				} else {
					$this->_extension = '';
				}

				unset($fileInfo);

				/** Копируем файл в рабочую директорию */
				if (copy($filePath,$this->_workPath.$this->_extension)) {

					/** Настраиваем soffice на конвертацию в PDF */
					$soffice = new Tools\SOffice();
					$pdfConv = $soffice->setExtention('pdf')
							->setOutputDir($this->_workDir)
							->setFilter('writer_pdf_Export');

					/** Конвертируем документ в PDF */
					if ($pdfConv->convert($this->_workPath.$this->_extension)) {
						
						/** Если задан текст водного знака создаем файл с водным знаком */
						if (!empty($this->_stampText) && $this->_makeStampHtml()) {

							/** Конвертируем водный знак в PDF */
							if ($pdfConv->convert($this->_workPath.'_stamp.html')) {
								$pdftk = new Tools\PDFtk();

								/** Поворачиваем водный знак на 90 градусов */
								if ($pdftk->rotate($this->_workPath.'_stamp.pdf',$this->_workPath.'_stamp_rotated.pdf','east',1)) {

									/** Накладываем водный знак на все страницы документа */
									$pdftk->multibackground($this->_workPath.'.pdf',$this->_workPath.'_stamp_rotated.pdf',$this->_workPath.'_final.pdf');

									return $this->_workPath.'_final.pdf';
								}
							}
						}

						rename($this->_workPath.'.pdf', $this->_workPath.'_final.pdf');
						return $this->_workPath.'_final.pdf';
					}
				}
			}
		}
	}

	public function __destruct()
	{
		if ($this->_cleanAfter) {
			$arFiles = [
				$this->_extension,
				'.pdf',
				'_stamp.html',
				'_stamp.pdf',
				'_stamp_rotated.pdf',
			];

			foreach ($arFiles as $filePostfix) {
				unlink($this->_workPath.$filePostfix);
			}
		}
	}
}
?>